/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       10 Feb 2016     krobinso
 *
 */

/**
 * @returns {Void} Any or no return value
 */


function internalApproval() {
	/*
	 * return codes
	 * F = approved internally
	 * S = go through standard approval process
	 * G = go through standard approval process with Goergen
	 */
	
	var searchString = 'SUB', searchStringPkg = 'PKG';
	var returnCode = '';
	var standardDiscountThreshold = 0.5;
	var customSubThreshold = 1.2;
	var itemCount = 0, standardCost = 0, customerCost = 0, standardCostSum = 0, discountAmount = 0;
	var standardCostMultiplied = 0, amount = 0, discountedValue = 0, multipliedAmount = 0;
	var validTran = false, customSub = false, customImp = false;
	var rec = null, itemType = null, itemId = null, locationId = null, subsidiaryId = null, result = null, itemText = null;
	var searchResultRow = null, cols = null, filters = null;
	var quoteId = nlapiGetRecordId();
	
	if (quoteId != null) {
		
		validTran = true;
		try {
			rec = nlapiLoadRecord('estimate', quoteId);
		} catch(e) {
			validTran = false;
		}

		if (validTran) {
                        returnCode = 'F';
			itemCount = rec.getLineItemCount('item');	
			
			if (itemCount > 0) {
				
				locationId = nlapiGetFieldValue('location');
				subsidiaryId = nlapiGetFieldValue('subsidiary');
				
				for (var item = 1; item <= itemCount; item++) {
					
					itemText = rec.getLineItemText('item','item',item); 
					itemId = rec.getLineItemValue('item','item',item);
					itemType = nlapiLookupField('item',itemId,'type');
					customSub = nlapiLookupField('item',itemId,'custitem_customsubscription');
                                        customImp = nlapiLookupField('item',itemId,'custitem_cusimplementationfee');
					amount = rec.getLineItemValue('item','amount',item);
					quantity = rec.getLineItemValue('item','quantity',item);
					
					if (itemType != null && itemType == 'Assembly' && itemText.indexOf(searchStringPkg) == -1) {

						filters = new Array();
						filters[0] = new nlobjSearchFilter('item', null, 'is', itemId);
						filters[1] = new nlobjSearchFilter('location', null, 'is', locationId);
						filters[2] = new nlobjSearchFilter('subsidiary', null, 'is', subsidiaryId);
						result = nlapiSearchRecord('transaction','customsearch_standardcost',filters);

						if (result != null) {
							searchResultRow = result[0];
						    cols = searchResultRow.getAllColumns();
						    standardCost = searchResultRow.getValue(cols[5]);
						    standardCostMultiplied = (standardCost * quantity);
						} else {
							standardCost = 0;
						}
						
						standardCostSum = Number(standardCostSum) + Number(standardCostMultiplied);
					}
					
					if (itemType != null && itemType == 'Service') {

						if (customImp == 'T') {
							returnCode = 'S';
						}

						if (amount != null)
							customerCost = amount;
						else
							customerCost = 0;
						
						if (customSub == 'T' && itemText.indexOf(searchString) > -1) {
							
							if (amount != null) {
								try {
									multipliedAmount = (standardCostSum * customSubThreshold);
								} catch(e) {
									multipliedAmount = 0;
								}
								
								/*if (amount <= standardCostSum ) {
									returnCode = 'G';
									break;
								}*/
								
								if (amount <= multipliedAmount ) {
									returnCode = 'G';
								} else {
									returnCode = 'S';
								}
							}
							
							standardCostSum = 0;
						}
						
						if (customSub == 'F' && itemId.indexOf(searchString) > -1)
							standardCostSum = 0;
					}
					
					if (itemType != null && itemType == 'Discount') {

						if (amount != null)
							discountAmount = amount;
						else
							discountAmount = 0;
						
						if (discountAmount < 0) {
							try {
								discountedValue = (Math.abs(discountAmount) / customerCost);
							} catch(e) {
								discountedValue = 0;
							}
							
							if (discountedValue >= standardDiscountThreshold ) {
								returnCode = 'G';
								break;
							} else {
								returnCode = 'S';
							}
						}
					}
					
				}

			}
			
		}
			
	}
	
	return returnCode;
}


function setRate() {

	/*var quoteId = nlapiGetRecordId();
	var rateCount = 0;
	var rateHolder = '', recQuote = null;
	
	if (quoteId != null) {
		validTran = true;
		try {
			//var currentRateHolder = nlapiGetFieldValue('custbody_rate');
			recQuote = nlapiLoadRecord('estimate', quoteId);

		} catch(e) {
			validTran = false;
			rateHolder = '-99';
		}
			
		if (validTran) {
			var itemCount = recQuote.getLineItemCount('item');
			
			if (itemCount > 0) {
				for (var item = 1; item <= itemCount; item++) {
					var multiplierFlag = nlapiLookupField('item', recQuote.getLineItemValue('item','item',item), 'custitem_enableratemultiplier');
					var billingScheduleId = recQuote.getLineItemValue('item','billingschedule',item);
					
					if (billingScheduleId != null && multiplierFlag == 'T') {
						multiplier = nlapiLookupField('billingschedule',billingScheduleId,'recurrencecount');
						currentRate = recQuote.getLineItemValue('item','rate',item);
						
						if (rateCount == 0)
							rateHolder = currentRate;
						else
							rateHolder = rateHolder + '|' + currentRate;
						
						rateCount++;
					}
					
				}
			}
			
		}
	}

	return rateHolder;*/
	return '-99';
}
