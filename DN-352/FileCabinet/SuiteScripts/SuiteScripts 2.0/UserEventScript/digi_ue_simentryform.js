/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope public
 * @param redirect
 * @param record
 */

require.config({
	paths : {
		"itmfulfillservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_itmfulfillservice"
	}
});

define(['N/redirect','N/record','N/runtime','itmfulfillservice', 'N/url'],

function(redirect, record, runtime, itmfulfillservice, url) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	try {
    		itmfulfillservice.addDeviceSetupButton(scriptContext);
    	} catch (e) {
            var ex = JSON.parse(e);
            var errorType = ex.type;
            var errorMsg = 	'Error: ' + ex.name + '\n' +
            				'Message: ' + ex.message;
            
            if (errorType == 'error.SuiteScriptError') {
            	errorMsg = errorMsg + '\n' +
            				'ID: ' + ex.Id + '\n' +
            				'Cause: ' + ex.cause + '\n' +
            				'Stack Trace: ' + ex.stack;
            }
            
            if (errorType == 'error.UserEventError') {
            	errorMsg = errorMsg + '\n' +
            				'ID: ' + ex.Id + '\n' +
            				'Event Type: ' + ex.eventType + '\n' +
            				'Record ID: ' + ex.recordId + '\n' +
            				'Stack Trace: ' + ex.stack;
            }
            
            log.debug(errorType, errorMsg);
    	}
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	try {
    		var itmfulRecord = scriptContext.newRecord;
    		var itmfulId = itmfulRecord.getValue('id');
    		var redirectDeviceWizard = itmfulfillservice.processDeviceSetup(itmfulRecord);
    		
    		if (redirectDeviceWizard) {
        		redirect.toSuitelet({
        			scriptId : 'customscript_digi_sl_devicesetupform',
        			deploymentId : 'customdeploy_digi_sl_devicesetupform',
        			parameters : {
        				digi_itmfulid : itmfulId
        			}
        		});
    		}
    	} catch (e) {
            var ex = JSON.parse(e);
            var errorType = ex.type;
            var errorMsg = 	'Error: ' + ex.name + '\n' +
            				'Message: ' + ex.message;
            
            if (errorType == 'error.SuiteScriptError') {
            	errorMsg = errorMsg + '\n' +
            				'ID: ' + ex.Id + '\n' +
            				'Cause: ' + ex.cause + '\n' +
            				'Stack Trace: ' + ex.stack;
            }
            
            if (errorType == 'error.UserEventError') {
            	errorMsg = errorMsg + '\n' +
            				'ID: ' + ex.Id + '\n' +
            				'Event Type: ' + ex.eventType + '\n' +
            				'Record ID: ' + ex.recordId + '\n' +
            				'Stack Trace: ' + ex.stack;
            }
            
            log.debug(errorType, errorMsg);
    	}
    }   

    return {
        beforeLoad: beforeLoad,
        //beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
