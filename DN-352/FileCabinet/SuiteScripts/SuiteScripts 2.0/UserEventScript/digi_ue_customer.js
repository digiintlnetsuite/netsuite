/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

require.config({
	paths : {
		"entityservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_entityservice"
	}
});

define(['N/error', 'N/log', 'entityservice'],

function(error, log, entityservice) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
    	try {
	    	var custNewRecord = scriptContext.newRecord;
	    	entityservice.mapGeoCodes(custNewRecord);  	
    	} catch (e) {
            var ex = JSON.parse(e);
            var errorType = ex.type;
            var errorMsg = 	'Error: ' + ex.name + '\n' +
            				'Message: ' + ex.message;
            if (errorType == 'error.SuiteScriptError') {
            	errorMsg = errorMsg + '\n' + 'ID: ' + ex.Id + '\n' + 'Cause: ' + ex.cause + '\n' + 'Stack Trace: ' + ex.stack;
            }
            if (errorType == 'error.UserEventError') {
            	errorMsg = errorMsg + '\n' + 'ID: ' + ex.Id + '\n' + 'Event Type: ' + ex.eventType + '\n' + 'Record ID: ' + ex.recordId + '\n' + 'Stack Trace: ' + ex.stack;
            }
            log.debug(errorType, errorMsg);
    	}
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {

    }

    return {
        //beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        //afterSubmit: afterSubmit
    };
    
});
