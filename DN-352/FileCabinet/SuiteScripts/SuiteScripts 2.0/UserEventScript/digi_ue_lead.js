/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope public
 */

require.config({
	paths : {
		"entityservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_entityservice"
	}
});

define(['N/error', 'N/log', 'entityservice'],

function(error, log, entityservice) {
   
    function beforeLoad(scriptContext) {

    }

    function beforeSubmit(scriptContext) {
    	try {
	    	var custNewRecord = scriptContext.newRecord;
	    	entityservice.setBdr(custNewRecord);
	    	entityservice.mapGeoCodes(custNewRecord);  	
    	} catch (e) {
            var ex = JSON.parse(e);
            var errorType = ex.type;
            var errorMsg = 	'Error: ' + ex.name + '\n' +
            				'Message: ' + ex.message;
            if (errorType == 'error.SuiteScriptError') {
            	errorMsg = errorMsg + '\n' + 'ID: ' + ex.Id + '\n' + 'Cause: ' + ex.cause + '\n' + 'Stack Trace: ' + ex.stack;
            }
            if (errorType == 'error.UserEventError') {
            	errorMsg = errorMsg + '\n' + 'ID: ' + ex.Id + '\n' + 'Event Type: ' + ex.eventType + '\n' + 'Record ID: ' + ex.recordId + '\n' + 'Stack Trace: ' + ex.stack;
            }
            log.debug(errorType, errorMsg);
    	}
    }

    function afterSubmit(scriptContext) {

    }

    return {
        //beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        //afterSubmit: afterSubmit
    };
    
});
