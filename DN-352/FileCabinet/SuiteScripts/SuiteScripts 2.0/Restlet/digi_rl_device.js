/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */

require.config({
	paths : {
		"searchservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_searchservice"
	},
});

define(['searchservice','N/record'],

function(searchservice, record) {
   
    /**
     * Function called upon sending a GET request to the RESTlet.
     *
     * @param {Object} requestParams - Parameters from HTTP request URL; parameters will be passed into function as an Object (for all supported content types)
     * @returns {string | Object} HTTP response body; return string when request Content-Type is 'text/plain'; return Object when request Content-Type is 'application/json'
     * @since 2015.1
     */
    function doGet(requestParams) {
    	log.debug('requestParams',requestParams);
    	var itmfulfilid = requestParams.itmfulfilid;
    	
    	var searchInventoryDetail = searchservice.simIdSearch(itmfulfilid);
    	var deviceArray = new Array();
    	searchInventoryDetail.run().each(function(result) {
    		
    		if (!result.getValue('custrecord_digi_device_configured'))
    			deviceArray.push(result);
    		
    		return true;
        });
    	
    	return deviceArray;
    }

    /**
     * Function called upon sending a PUT request to the RESTlet.
     *
     * @param {string | Object} requestBody - The HTTP request body; request body will be passed into function as a string when request Content-Type is 'text/plain'
     * or parsed into an Object when request Content-Type is 'application/json' (in which case the body must be a valid JSON)
     * @returns {string | Object} HTTP response body; return string when request Content-Type is 'text/plain'; return Object when request Content-Type is 'application/json'
     * @since 2015.2
     */
    function doPut(requestBody) {
        var restletData = requestBody.data;
        var stringData = JSON.stringify(requestBody);
        
        log.debug('stringData',stringData);
        if (requestBody.recordType && requestBody.internalId) {
        	
            var objRecord = record.load({
            	type: requestBody.recordType,
            	id: requestBody.internalId,
            	isDynamic: true
            });
        	
        	for (var objectType in restletData) {
        		var objectData = restletData[objectType];
                for ( var key in objectData) {               	
                    if (objectData.hasOwnProperty(key)) {
                       objRecord.setValue({
                          fieldId : key,
                          value : objectData[key]
                       });
                    }
                 }
                 
                 var recordId = objRecord.save({
                    enableSourcing : false,
                    ignoreMandatoryFields : true
                 });
        	}
        }
                
        return stringData
    }


    /**
     * Function called upon sending a POST request to the RESTlet.
     *
     * @param {string | Object} requestBody - The HTTP request body; request body will be passed into function as a string when request Content-Type is 'text/plain'
     * or parsed into an Object when request Content-Type is 'application/json' (in which case the body must be a valid JSON)
     * @returns {string | Object} HTTP response body; return string when request Content-Type is 'text/plain'; return Object when request Content-Type is 'application/json'
     * @since 2015.2
     */
    function doPost(requestBody) {

    }

    /**
     * Function called upon sending a DELETE request to the RESTlet.
     *
     * @param {Object} requestParams - Parameters from HTTP request URL; parameters will be passed into function as an Object (for all supported content types)
     * @returns {string | Object} HTTP response body; return string when request Content-Type is 'text/plain'; return Object when request Content-Type is 'application/json'
     * @since 2015.2
     */
    function doDelete(requestParams) {

    }

    return {
        'get': doGet,
        put: doPut,
        post: doPost,
        'delete': doDelete
    };
    
});
