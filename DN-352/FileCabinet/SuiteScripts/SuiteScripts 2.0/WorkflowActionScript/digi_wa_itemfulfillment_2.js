/**
 * @NApiVersion 2.x
 * @NScriptType workflowactionscript
 */
var itemSearchArray = ["FT-GTW"];

require.config({
	paths : {
		"searchservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_searchservice"
	}
});

define(['searchservice'],

function(searchservice) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @Since 2016.1
     */
    function onAction(scriptContext) {
        try {
            var itemFulfillRecord = scriptContext.newRecord;
            var createdfrom = itemFulfillRecord.getValue('createdfrom');
            log.debug(createdfrom);
        	
        	var searchResult = searchservice.searchSalesOrder(createdfrom, itemSearchArray);
            var firstValue = searchResult.run().getRange({ start: 0, end: 1 });
            
            if (firstValue.length > 0)
            	return 'T';
            else
            	return 'F';
        	
        } catch (e) {
        	return 'F';
        }
    }

    return {
        onAction : onAction
    };
    
});
