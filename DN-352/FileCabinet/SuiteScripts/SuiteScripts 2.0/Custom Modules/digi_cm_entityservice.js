/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

require.config({
	paths : {
		"searchservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_searchservice"
	}
});

define(['N/record', 'N/search', 'searchservice'],

function(record, search, searchservice) {
   
	function mapGeoCodes(custNewRecord) {
		var country = 'US';
		
    	if (custNewRecord.getValue('billcountry'))
    		country = custNewRecord.getValue('billcountry');
    	else if (custNewRecord.getValue('shipcountry'))
    		country = custNewRecord.getValue('shipcountry');
    	
    	if (country) {
    		var geoResults = searchservice.geoMappingByCountry(country);
    		
            var firstValue = geoResults.run().getRange({start: 0, end: 1});
            if (firstValue.length > 0) {
            	var currentSubGeo = custNewRecord.getValue('custentity_digi_sub_geo');
        		var subGeo = firstValue[0].getValue('custrecord_digi_sub_geo');
            	if (currentSubGeo != subGeo)
            		custNewRecord.setValue('custentity_digi_sub_geo', subGeo);
        		
            	var currentGeo = custNewRecord.getValue('custentity_digi_geo');
        		var geo = firstValue[0].getValue('custrecord_digi_geo')
            	if (currentGeo != geo)
            		custNewRecord.setValue('custentity_digi_geo', geo);
            }
    	}
	}
	
	function setBdr(custNewRecord) {
    	var salesRepId = custNewRecord.getValue('salesrep');
    	if (salesRepId != custNewRecord.getValue('custentity_digi_businessdevrep')) {
    		var bdrEmployees = searchservice.getSavedSearch('customsearch_digi_bdrgroup');
    		bdrEmployees.filters.push(search.createFilter( {name: 'internalid', operator: search.Operator.IS, values: salesRepId} ) );
        	
    		var resultFound = false, bdrId;
    		bdrEmployees.run().each(function(result) {
    			resultFound = true;
        		bdrId = result.getValue({name: 'internalid'});
        	});
        	if (resultFound)
        		custNewRecord.setValue('custentity_digi_businessdevrep', bdrId);
    	}
	}
	
    return {
    	mapGeoCodes : mapGeoCodes,
    	setBdr : setBdr
    };
    
});
