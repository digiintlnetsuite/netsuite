/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */
define(['N/record'],

function(record) {
   
	function itemExits(context, findItemId) {

    	var itemCount = context.getLineCount({
    		sublistId: 'item'
    	});
    	   	
		for (var item = 0; item < itemCount; item++) {
        	var itemId = context.getSublistValue({
        		sublistId: 'item',
        		fieldId: 'item',
        		line: item
        	});
        	
        	if (findItemId == itemId)
        		return true;
    	}
    	
    	return false;
	}
	
	function itemExitsText(context, findItemIdText) {

    	var itemCount = context.getLineCount({
    		sublistId: 'item'
    	});
    	   	
		for (var item = 0; item < itemCount; item++) {
        	var itemIdText = context.getSublistText({
        		sublistId: 'item',
        		fieldId: 'itemname',
        		line: item
        	});
        	
        	if (findItemIdText.indexOf(itemIdText) != -1)
        		return true;
    	}
    	
    	return false;
	}
		
    return {
    	itemExits : itemExits,
    	itemExitsText : itemExitsText
    };
    
});
