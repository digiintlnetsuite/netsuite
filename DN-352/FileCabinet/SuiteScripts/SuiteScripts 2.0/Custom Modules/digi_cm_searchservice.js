/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

require.config({
	paths : {
		"objectservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_objectservice"
	}
});

define(['N/search','N/record','objectservice'],

function(search,record,objectservice) {
	
	function simIdSearchById(itemFulfillId) {
        var simidSearch = search.create({
        	type: 'customrecord_digi_simids',
            columns: [{
                name: 'custrecord_digi_simid',
            }, {
            	name: 'custrecord_digi_serial_number'
            }, {
            	name: 'custrecord_digi_itemfulfillment'
            }, {
            	name: 'internalid'
            }, {
            	name: 'custrecord_digi_connectware_id'
            }, {
            	name: 'custrecord_digi_sn_provisioned'
            }, {
            	name: 'custrecord_digi_device_configured'
            }],
            filters: [{
                name: 'custrecord_digi_itemfulfillment',
                operator: 'is',
                values: itemFulfillId
            },{
                name: 'isinactive',
                operator: 'is',
                values: false
            }]
        });
        
        return simidSearch;
	}
	
	function getAssemblyItem(internalId) {
        var itemSearch = search.create({
        	type: search.Type.ASSEMBLY_ITEM,
            columns: [{
                name: 'itemid',
            }],
            filters: [{
                name: 'internalid',
                operator: 'is',
                values: internalId
            }]
        });
        
        return itemSearch;
	}
	
	function searchInventoryDetail(itemFulfillId, itemSearchArray) {
		
		var salesOrderSearch = search.create({
	        type: search.Type.ITEM_FULFILLMENT,
	        columns: [{
	            name: 'inventorynumber',
	            join : 'inventoryDetail',
	            label : 'inventory number'
	        }, {
	        	name: 'mainline'
	        }, {
	        	name: 'Type'
	        }, {
	        	name: 'internalid',
	        	join: 'item'
	        }, {
	        	name: 'itemid',
	        	join: 'item'
	        }],
	        filters: [{
	            name: 'itemid',
	            join: 'item',
	            operator: 'is',
	            values: itemSearchArray
	        }, {
	            name: 'internalid',
	            operator: 'is',
	            values: itemFulfillId
	        }]
        });
        
        return salesOrderSearch;
	}
	
	function getSavedSearch(savedSearchType) {
        var savedSearch = search.load({
        	id: savedSearchType
        });
        
        return savedSearch;
	}
	
	function getCredentials(name) {
		var credentials = search.create({
	        type: 'customrecord_digi_credentials',
	        columns: [{
	            name: 'name'
	        }, {
	        	name: 'internalid'
	        }],
	        filters: [{
	            name: 'name',
	            operator: 'is',
	            values: name
	        }]
        });
        
		return credentials;
	}
	
	function loadCredentials(credentialName) {
		var internalid;
		var credentialsResults = getCredentials(credentialName);
		credentialsResults.run().each(function(result) {
        	internalid = result.getValue('internalid');
        	return true;
        });

    	var credentials = record.load({
    		type: 'customrecord_digi_credentials',
    		id: internalid
    	});
    	
    	var credentialsObject = new objectservice.credentials();
    	credentialsObject.username = credentials.getValue({fieldId: 'custrecord_digi_username'});
    	credentialsObject.password = credentials.getValue({fieldId: 'custrecord_digi_password'});
    	credentialsObject.host = credentials.getValue({fieldId: 'custrecord_digi_host'}); 
    	credentialsObject.token = credentials.getValue({fieldId: 'custrecord_digi_validation_token'});
    	
    	return credentialsObject;
	}
	
	function simIdSearch(itemFulfillId) {
        var simidSearch = search.create({
        	type: 'customrecord_digi_simids',
            columns: [{
                name: 'custrecord_digi_simid',
            }, {
            	name: 'custrecord_digi_serial_number'
            }, {
            	name: 'custrecord_digi_itemfulfillment'
            }, {
            	name: 'internalid'
            }, {
            	name: 'custrecord_digi_connectware_id'
            }, {
            	name: 'custrecord_digi_sn_provisioned'
            }],
            filters: [{
                name: 'custrecord_digi_itemfulfillment',
                operator: 'is',
                values: itemFulfillId
            }, {
                name: 'custrecord_digi_device_configured',
                operator: 'is',
                values: false
            },{
                name: 'isinactive',
                operator: 'is',
                values: false
            }]
        });
        
        return simidSearch;
	}
	
	function searchSalesOrder(salesOrderId, itemSearchArray) {
		
		var salesOrderSearch = search.create({
	        type: search.Type.SALES_ORDER,
	        columns: [{
	        	name: 'internalid',
	        	join: 'item'
	        }, {
	        	name: 'itemid',
	        	join: 'item'
	        }],
	        filters: [{
	            name: 'itemid',
	            join: 'item',
	            operator: 'is',
	            values: itemSearchArray
	        }, {
	            name: 'internalid',
	            operator: 'is',
	            values: salesOrderId
	        }]
        });
        
        return salesOrderSearch;
	}
	
	function geoMappingByCountry(country) {
        var geoMapping = search.create({
        	type: 'customrecord_digi_countrytogeomapping',
            columns: [{
                name: 'custrecord_digi_country',
            }, {
            	name: 'custrecord_digi_sub_geo'
            }, {
            	name: 'custrecord_digi_geo'
            }, {
            	name: 'custrecord_digi_countryshortname'
            }],
            filters: [{
                name: 'custrecord_digi_countryshortname',
                operator: 'is',
                values: country
            }]
        });
        
        return geoMapping;
	}
	
    return {
    	simIdSearch : simIdSearch,
    	searchInventoryDetail : searchInventoryDetail,
    	getSavedSearch : getSavedSearch,
    	getCredentials : getCredentials,
    	loadCredentials : loadCredentials,
    	simIdSearchById : simIdSearchById,
    	searchSalesOrder : searchSalesOrder,
    	geoMappingByCountry : geoMappingByCountry
    };
    
});
