/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

require.config({
	paths : {
		"searchservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_searchservice"
	}
});

require.config({
	paths : {
		"objectservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_objectservice"
	}
});

define(['N/https','N/encode', 'N/error', 'N/record', 'searchservice','objectservice'],

function(https, encode, error, record, searchservice, objectservice) {
	
	function postDeviceCore(deviceIdArray, credentialName) {
		
        var deviceCoreArray = new Array();
        for (var x = 0; x < deviceIdArray.length; x++) {
            var deviceCoreobject = new objectservice.deviceCoreReq();
            deviceCoreobject.id = deviceIdArray[x];
            deviceCoreArray.push(deviceCoreobject);
        }
		log.debug('deviceCoreArray',JSON.stringify(deviceCoreArray));
		
		var credentialsObject = searchservice.loadCredentials(credentialName);    	
    	var stringInput = credentialsObject.username + ':' + credentialsObject.password;
    	var credentials = encode.convert({string: stringInput, inputEncoding: encode.Encoding.UTF_8, outputEncoding: encode.Encoding.BASE_64});
    	var headers = {'Authorization': 'Basic ' + credentials,'context-type':'application/json'};

    	var response = https.post({
			url: credentialsObject.host + '/ws/v1/devices/inventory.json',
			headers: headers,
			body: JSON.stringify(deviceCoreArray)
		});
		
		return response;
	}
	
	function devIdToConnWareId(deviceId) {
		var connWarPrefix = '00000000-00000000-';
		var connWarMiddle = 'FF-FF';
		var connWareId = 'invalid';
		
		if (deviceId.length == 12) {
			connWareId = connWarPrefix + deviceId.substring(0,6) + connWarMiddle + deviceId.substring(6);
		}
		
		return connWareId;
	}
	
	function connWareIdToDevId(connectwareId) {
		var deviceId = 'invalid';
		
		if (connectwareId.length == 35) {
			var TempStr = connectwareId.substring(18);
			deviceId = TempStr.substring(0,6) + TempStr.substring(11);
		}
		
		return deviceId;
	}
	   
    return {
        postDeviceCore : postDeviceCore,
        devIdToConnWareId : devIdToConnWareId,
        connWareIdToDevId : connWareIdToDevId
    };
    
});
