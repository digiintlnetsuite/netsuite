/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

var deviceSetupItemArray = ["FT-GTW"];

require.config({
	paths : {
		"searchservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_searchservice"
	}
});

require.config({
	paths : {
		"dcservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_dcservice"
	}
});

require.config({
	paths : {
		"objectservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_objectservice"
	}
});

define(['N/redirect','N/record','N/runtime','searchservice','dcservice','objectservice', 'N/url'],
		
function(redirect, record, runtime, searchservice, dcservice, objectservice, url) {
		
    function processDeviceSetup(itmfulRecord) {
    	try {
    		var redirectDeviceWizard = false;
    		var itmfulId = itmfulRecord.getValue('id');

    		//Get existing device setup records.
    		existingDeviceArray = getExistingDevices(itmfulId);

    		//Get serial numbers to provision.
    		var deviceIdArrayProvision = new Array();
    		var fulFillSns = new Array();
    		var searchInventoryDetail = searchservice.searchInventoryDetail(itmfulId, deviceSetupItemArray);
        	searchInventoryDetail.run().each(function(result) {
                var serialNumberText = result.getText({name: 'inventorynumber', join: 'inventoryDetail'});
        		if (serialNumberText) {
        			var provision = true;
                	for (var x = 0; x < existingDeviceArray.length; x++) {
                		if (existingDeviceArray[x].serialNumber == serialNumberText && existingDeviceArray[x].provisioned)
                			provision = false;
                	}
                	
                	if(provision)
                		deviceIdArrayProvision.push(dcservice.devIdToConnWareId(serialNumberText));
                	
                	fulFillSns.push(serialNumberText);
        		}
        		
        		redirectDeviceWizard = true;
            	return true;
            });
        	
        	//Provision serial numbers.
        	markProcessed = provisionSnDevice(deviceIdArrayProvision, 'FTGTW');
        	
        	//Upsert device setup records.
        	upsertDeviceSetup(markProcessed, existingDeviceArray, itmfulId);
        	
        	//Remove device setup records.
        	removeDeviceSetup(fulFillSns, existingDeviceArray);
        	
        	return redirectDeviceWizard;
    	} catch (e) {
            var ex = JSON.parse(e);
            var errorType = ex.type;
            var errorMsg = 	'Error: ' + ex.name + '\n' +
            				'Message: ' + ex.message;
            
            if (errorType == 'error.SuiteScriptError') {
            	errorMsg = errorMsg + '\n' +
            				'ID: ' + ex.Id + '\n' +
            				'Cause: ' + ex.cause + '\n' +
            				'Stack Trace: ' + ex.stack;
            }
            
            if (errorType == 'error.UserEventError') {
            	errorMsg = errorMsg + '\n' +
            				'ID: ' + ex.Id + '\n' +
            				'Event Type: ' + ex.eventType + '\n' +
            				'Record ID: ' + ex.recordId + '\n' +
            				'Stack Trace: ' + ex.stack;
            }
            
            log.debug(errorType, errorMsg);
    	}
    }
    
    function removeDeviceSetup(fulFillSns, existingDeviceArray) {
    	log.debug('fulFillSns',fulFillSns);
    	log.debug('existingDeviceArray',existingDeviceArray);
    	
    	var removeSnArray = new Array();
    	for (var x = 0; x < existingDeviceArray.length; x++) {
    		if (fulFillSns.indexOf(existingDeviceArray[x].serialNumber) == -1)
    			removeSnArray.push(existingDeviceArray[x].internalId);
    	}
    	
    	if (removeSnArray.length > 0) {
    		for (var x = 0; x < removeSnArray.length; x++) {
				var objRecord = record.load({
		        	type: 'customrecord_digi_simids',
		        	id: removeSnArray[x],
		        	isDynamic: true
		        });
							
				objRecord.setValue({fieldId: 'isinactive',value: true});
		        var recordId = objRecord.save({
		        	enableSourcing : false,
		        	ignoreMandatoryFields : true
		        });
    		}
    	}
    	
    	log.debug('removeSnArray',removeSnArray);
    }
    
	function getExistingDevices(itmfulId) {
		var existingDeviceArray = new Array();
		var simidSearch = searchservice.simIdSearchById(itmfulId);
		
        simidSearch.run().each(function(result) {
        	var connectwareMap = new objectservice.connectwareMap();
        	connectwareMap.serialNumber = result.getValue('custrecord_digi_serial_number');
        	connectwareMap.connectware_id = result.getValue('custrecord_digi_connectware_id');
        	connectwareMap.internalId = result.getValue('internalid');
            if (result.getValue('custrecord_digi_sn_provisioned')) {
            	connectwareMap.provisioned = true;
            } else {
            	connectwareMap.provisioned = false;
            }
            
            if (result.getValue('custrecord_digi_device_configured')) {
            	connectwareMap.configured = true;
            } else {
            	connectwareMap.configured = false;
            }
            existingDeviceArray.push(connectwareMap);
        	return true;
    	});
        
        return existingDeviceArray;
	}
	
	function provisionSnDevice(deviceIdArrayProvision, credentialName) {
		var markProcessed = new Array();
		if(deviceIdArrayProvision.length == 0)
    		return markProcessed;
    		
		var response = dcservice.postDeviceCore(deviceIdArrayProvision, credentialName);
		var jsonObject = JSON.parse(response.body);
		if (jsonObject.list.length == 0)
			return markProcessed;
		
		for (var x = 0; x < jsonObject.list.length; x++) {
			var jsonStr = JSON.stringify(jsonObject.list[x]);
			var deviceCoreResponse = new objectservice.deviceCoreResponse();
			if (jsonStr.indexOf('error_status') != -1) {
				deviceCoreResponse.error_status = jsonObject.list[x].error_status;
				deviceCoreResponse.error_message = jsonObject.list[x].error_message;
				deviceCoreResponse.id = jsonObject.list[x].error_context.id;
				deviceCoreResponse.serial_number = dcservice.connWareIdToDevId(jsonObject.list[x].error_context.id);
				deviceCoreResponse.device_error = true;
				markProcessed.push(deviceCoreResponse);
			} else {
				deviceCoreResponse.id = jsonObject.list[x].id;
    			deviceCoreResponse.serial_number = dcservice.connWareIdToDevId(jsonObject.list[x].id);
    			deviceCoreResponse.device_error = false;
    			markProcessed.push(deviceCoreResponse);
			}
		}
		
		return markProcessed;
	}
	
	function upsertDeviceSetup(markProcessed, existingDeviceArray, itmfulId) {
    	if(markProcessed.length == 0)
    		return 'No records to upsert.';
		
		for (var i = 0; i < markProcessed.length; i++) {
			var createRecord = true, existingInteralId;
			var connectWareIdProcessed = dcservice.devIdToConnWareId(markProcessed[i].serial_number);
        	for (var x = 0; x < existingDeviceArray.length; x++) {
        		if (connectWareIdProcessed == existingDeviceArray[x].connectware_id) {
        			createRecord = false;
        			existingInteralId = existingDeviceArray[x].internalId;
        			break;
        		}
        	}
			if(createRecord) {
                var objRecord = record.create({
                	type: 'customrecord_digi_simids',
                	isDynamic: true
                });
                objRecord.setValue({fieldId: 'custrecord_digi_itemfulfillment',value: itmfulId});
                objRecord.setValue({fieldId: 'custrecord_digi_serial_number',value: markProcessed[i].serial_number});
                
                if (markProcessed[i].device_error)
                	objRecord.setValue({fieldId: 'custrecord_digi_sn_provisioned',value: false});
                else
                	objRecord.setValue({fieldId: 'custrecord_digi_sn_provisioned',value: true});
                
                objRecord.setValue({fieldId: 'custrecord_digi_connectware_id',value: markProcessed[i].id});
                objRecord.setValue({fieldId: 'custrecord_digi_json_blob',value: JSON.stringify(markProcessed[i])});

                var recordId = objRecord.save({
                	enableSourcing : false,
                	ignoreMandatoryFields : true
                });
			} else {
				var objRecord = record.load({
                	type: 'customrecord_digi_simids',
                	id: existingInteralId,
                	isDynamic: true
                });
				
                if (markProcessed[i].device_error)
                	objRecord.setValue({fieldId: 'custrecord_digi_sn_provisioned',value: false});
                else
                	objRecord.setValue({fieldId: 'custrecord_digi_sn_provisioned',value: true});
				
				objRecord.setValue({fieldId: 'custrecord_digi_json_blob',value: JSON.stringify(markProcessed[i])});
                var recordId = objRecord.save({
                	enableSourcing : false,
                	ignoreMandatoryFields : true
                });
                        					
			}
		}
	}
	
	function addDeviceSetupButton(scriptContext) {
		var itmfulRecord = scriptContext.newRecord;
		var itmfulId = itmfulRecord.getValue('id');
		existingDeviceArray = getExistingDevices(itmfulId);
		if(existingDeviceArray.length > 0) {
			var form = scriptContext.form;        
	        var deviceSetupUrl = url.resolveScript({
	                scriptId : 'customscript_digi_sl_devicesetupform',
	                deploymentId : 'customdeploy_digi_sl_devicesetupform',
	                returnExternalUrl : false,
	                params : { "digi_itmfulid" : itmfulId }
	            });
	        
	        form.addButton({
	            "id": "custpage_btn_devicesetup",
	            "label": "Device Setup",
	            "functionName" : 'window.parent.location.assign(\'' + deviceSetupUrl + '\');'
	        });
		}
	}
   
    return {
    	processDeviceSetup : processDeviceSetup,
    	getExistingDevices : getExistingDevices,
    	provisionSnDevice : provisionSnDevice,
    	upsertDeviceSetup : upsertDeviceSetup,
    	addDeviceSetupButton : addDeviceSetupButton
    };
    
});
