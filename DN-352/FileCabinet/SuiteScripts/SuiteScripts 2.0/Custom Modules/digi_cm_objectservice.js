/**
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

define([],

function() {
   	
	function deviceCoreReq() {
		this.id;
		this.tags;
	}
	
	function deviceCoreResError() {
		this.error_status;
		this.error_message;
		this.id;
		this.serial_number;
	}
	
	function deviceCoreResSuccess() {
		this.channels_uri;
		this.metrics_uri;
		this.restricted_status;
		this.health_status;
		this.type;
		this.connection_status;
		this.management_uri;
		this.id;
		this.serial_number;
	}
	
	function deviceCoreResponse() {
		this.channels_uri;
		this.metrics_uri;
		this.restricted_status;
		this.health_status;
		this.type;
		this.connection_status;
		this.management_uri;
		this.id;
		this.serial_number;
		this.error_status;
		this.error_message;
		this.device_error;
	}
	
	function connectwareMap() {
		this.serialNumber;
		this.connectware_id;
		this.internalId;
		this.provisioned;
		this.configured;
	}
	
	function credentials() {
		this.username;
		this.password;
		this.host;
		this.token;
	}
		
    return {
    	deviceCoreReq : deviceCoreReq,
    	deviceCoreResError : deviceCoreResError,
    	deviceCoreResSuccess : deviceCoreResSuccess,
    	deviceCoreResponse : deviceCoreResponse,
    	connectwareMap : connectwareMap,
    	credentials : credentials
    };
    
});
