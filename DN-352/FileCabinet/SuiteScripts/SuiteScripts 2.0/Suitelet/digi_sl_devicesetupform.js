var ENCODEMODULE, RUNTIMEMODULE, UIMODULE, URLMODULE, DEPLOYMENT_URL;

/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

require.config({
	paths : {
		"searchservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_searchservice"
	}
});

define(['N/encode', 'N/runtime', 'N/ui/serverWidget', 'N/url', 'N/error', 'N/record', 'N/redirect', 'searchservice'],

function(encode, runtime, serverWidget, url, error, record, redirect, searchservice) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {

        try {
        	
            var request = context.request;
            var response = context.response;
        	
            if (context.request.method == 'GET') {
            	
            	var itemFulfillId = request.parameters.digi_itmfulid;
            	        		
        		var host = 'https://devicesetup-qa.digi.com';
        		var environment = JSON.stringify(runtime.envType);
        		if (environment == '"PRODUCTION"')
        			host = 'https://devicesetup.digi.com';
        		
                var credentialName = 'FTGTW'
            	var form = serverWidget.createForm({
                    title: 'Device Setup Wizard'
                });
                
                var itmfulfillIdField = form.addField({
	                id: 'digi_itmfulid',
	                type: serverWidget.FieldType.TEXT,
	                label: 'internalId'
                });
                itmfulfillIdField.defaultValue = itemFulfillId;
                if (itmfulfillIdField) {
                	itmfulfillIdField.updateDisplayType({
                        displayType: serverWidget.FieldDisplayType.HIDDEN
                    });
                }
                
                form.addSubmitButton({
                    label: 'Got To ItemFulfillment'
                });
            	            	
            	var inline2 = form.addField({
                    id:'custpage_trigger_it',
                    label:'not shown',
                    type: serverWidget.FieldType.INLINEHTML
                });
            	
        		var credentialsObject = searchservice.loadCredentials(credentialName);            	
            	var url = host +'?at=' + credentialsObject.token + '&id=' + itemFulfillId;
            	log.debug('url',url);
            	inline2.defaultValue = '<iframe style=\"display: block; height: 100vh; overflow-x:hidden; overflow-y:hidden; width: 85vw; border: none;\" scrolling=\"no\" src=\"' + url + '\"></iframe>';
            	            	
        		context.response.writePage(form);
        		return;
            }
            
            var itemFulfillId = request.parameters.digi_itmfulid;
    		redirect.toRecord({
    			type : record.Type.ITEM_FULFILLMENT,
    			id : itemFulfillId
    		});
        	
            return;
        } catch (e) {
            var ex = JSON.parse(e);
            var errorType = ex.type;
            var errorMsg = 	'Error: ' + ex.name + '\n' +
            				'Message: ' + ex.message;
            
            if (errorType == 'error.SuiteScriptError') {
            	errorMsg = errorMsg + '\n' +
            				'ID: ' + ex.Id + '\n' +
            				'Cause: ' + ex.cause + '\n' +
            				'Stack Trace: ' + ex.stack;
            }
            
            if (errorType == 'error.UserEventError') {
            	errorMsg = errorMsg + '\n' +
            				'ID: ' + ex.Id + '\n' +
            				'Event Type: ' + ex.eventType + '\n' +
            				'Record ID: ' + ex.recordId + '\n' +
            				'Stack Trace: ' + ex.stack;
            }
            
            log.debug(errorType, errorMsg);
        }
    	
    }
    
    return {
        onRequest: onRequest
    };
    
});
