/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Jan 2016     krobinso
 * 1.1		  05 Dec   2016     krobinso      Start using populate arm start date over key word 'SUB'
 *
 */

/**
 * @returns {Void} Any or no return value
 */

function resetBillingSchedule() {
	var salesOrderId = null;
	var searchString = 'SUB';
	var rec = null;
	
	try {
		salesOrderId = nlapiGetFieldValue('createdfrom');
		fieldText = nlapiGetFieldText('createdfrom');
	} catch(e) {
		salesOrderId = null; 
	}

	if (salesOrderId != null) {
		var validTran = true;
		
		try {
			rec = nlapiLoadRecord('salesorder', salesOrderId);
		} catch(e) {
			validTran = false; 
		}
		
		if (validTran) {
			var dateModified = false;
			var itemCount = 0;
			var currentStartdate = rec.getFieldValue('startdate');
			var currentTrandate = rec.getFieldValue('trandate');
			
			//var now = new Date(2015,11,1);
			var now = new Date();
			var month = now.getMonth();
			var year = now.getFullYear();
						
			if (month == 11) {
				month = 0;
				year = year + 1;
			} else {
				month = month + 1;
			}
	
			var strFirstNextMonth = nlapiDateToString(new Date(year, month , 1), 'date');
			if (currentStartdate != strFirstNextMonth) {
				rec.setFieldValue('startdate', strFirstNextMonth);
				itemCount = rec.getLineItemCount('item');
				dateModified = true;
			}
							
			if (itemCount > 0) {
				for (var item = 1; item <= itemCount; item++) {

					// KMR we now use 'populate arm start date'
					var popArmStartDate = nlapiLookupField('item',rec.getLineItemValue('item','item',item),'custitem_digi_populatearmstartdate');
					//var itemId = nlapiLookupField('item',rec.getLineItemValue('item','item',item),'itemid');
					
					if (popArmStartDate != null && popArmStartDate == 'T') {
					//if (itemId != null && itemId.indexOf(searchString) > -1) {
						dateModified = true;
						rec.setLineItemValue('item','custcol_digi_armstartdate', item ,strFirstNextMonth);
						rec.commitLineItem('item');
					}
				}						
			}
			
			var currentMonth = nlapiDateToString(now, 'date');
			if (currentTrandate != currentMonth) {
				rec.setFieldValue('trandate', currentMonth);
				dateModified = true;	
			}
				
			if (dateModified)
				nlapiSubmitRecord(rec, true);	
				
		}
	}
}
