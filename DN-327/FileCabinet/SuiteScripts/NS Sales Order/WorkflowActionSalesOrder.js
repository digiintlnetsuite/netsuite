/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Feb 2016     krobinso
 * 1.1        11 Jul   2016     jmaulik       Changed calculated ship date from 10 days to 15 days
 * 1.2		  28 Sep   2016     jmaulik       Removed calculated ship date logic
 * 1.3		  05 Dec   2016     krobinso      Start using populate arm start date over key word 'SUB'
 *
 */

/**
 * @returns {Void} Any or no return value
 */
function setSoSubAmount() {
	
	var searchString = 'SUB';
	//var shipDateBumpOut = 15;
	var validTran = false, lineItemUpdated = false;
	var rec = null, currentRate = null, source = null;	
	var salesOrderId = nlapiGetRecordId();
	
	if (salesOrderId != null) {
		var dateModified = false;
		shipCountry = nlapiGetFieldValue('shipcountry');
		
		validTran = true;
		try {
			rec = nlapiLoadRecord('salesorder', salesOrderId);
		} catch(e) {
			validTran = false; 
		}
		
		if (validTran) {
			var itemCount = rec.getLineItemCount('item');
			
			if (itemCount > 0) {
				var locationId = nlapiGetFieldValue('location');
				var subsidiaryId = nlapiGetFieldValue('subsidiary');
				for (var item = 1; item <= itemCount; item++) {
					
					currentRate = rec.getLineItemValue('item','rate',item);
					nlapiLogExecution('debug','currentRate', currentRate);
					
					if (currentRate == 0) {
						var itemId = rec.getLineItemValue('item','item',item);

						var filters = new Array();
						filters[0] = new nlobjSearchFilter('item', null, 'is', itemId);
						filters[1] = new nlobjSearchFilter('location', null, 'is', locationId);
						filters[2] = new nlobjSearchFilter('subsidiary', null, 'is', subsidiaryId);
						
						
						var result = nlapiSearchRecord('transaction','customsearch_standardcost',filters);
						nlapiLogExecution('debug','result', result);
						
						if (result != null) {
							var searchResultRow = result[0];
						    var cols = searchResultRow.getAllColumns();
						    var standardCost = searchResultRow.getValue(cols[5]);
						    
						    var outsideUs = false;
						    if (shipCountry != null && shipCountry != 'US')
						    	outsideUs = true;
						    
						    if (standardCost != null && outsideUs) {
						    	standardCost = (standardCost * 1.05);
						    	standardCost = Math.round(standardCost);
								rec.setLineItemValue('item','rate', item , standardCost);
								rec.setLineItemValue('item','amount', item , '0.00');
								rec.commitLineItem('item');
								lineItemUpdated = true;
						    }
						}
					}
										
				}
				
			}
		}
		
		if(validTran) {
			var now = new Date();
			var month = now.getMonth();
			var year = now.getFullYear();
			//var day = now.getDay();
			
			if (month == 11) {
				month = 0;
				year = year + 1;
			} else {
				month = month + 1;
			}
			
			var currentStartdate = rec.getFieldValue('startdate');
          
          	// 
			var strFirstNextMonth = nlapiDateToString(new Date(year, month , 1), 'date');
			if (currentStartdate != strFirstNextMonth) {
				dateModified = true;
			rec.setFieldValue('startdate', strFirstNextMonth);
			}
		
			var itemCount = rec.getLineItemCount('item');
			if (itemCount > 0) {
				for (var item = 1; item <= itemCount; item++) {

					// KMR we now use 'populate arm start date'
					//var itemId = nlapiLookupField('item',rec.getLineItemValue('item','item',item),'itemid');
					var popArmStartDate = nlapiLookupField('item',rec.getLineItemValue('item','item',item),'custitem_digi_populatearmstartdate');

					if (popArmStartDate != null && popArmStartDate == 'T') {
					//if (itemId != null && itemId.indexOf(searchString) > -1) {	
						dateModified = true;
						rec.setLineItemValue('item','custcol_digi_armstartdate', item ,strFirstNextMonth);
						rec.commitLineItem('item');
					}
				}						
			}

          	// JM Removed the ship date bump out logic 9/28/16
			//var fifteenDays = 1000 * 60 * 60 * 24 * shipDateBumpOut;
			//var addFifteenDays = new Date(new Date().getTime() + fifteenDays);
			//var strAddFifteenDays = nlapiDateToString(addFifteenDays , 'date');
			//rec.setFieldValue('shipdate', strAddFifteenDays );
			
			try {
				source = rec.getFieldValue('createdfrom');
			} catch(e) {
				source = null;
			}
			
			//nlapiLogExecution('debug','source', source);
			//if (source != null && source == 'Web Services') {
			//if (source == null) {
				//rec.setFieldValue('custbody_salesorderapproved','T');
			//}
			
            dateModified = true;
		}
		
		if (lineItemUpdated || dateModified)
			nlapiSubmitRecord(rec, true);
	}
			

}

function addWebStoreKitSub() {
		
	var salesOrderId = nlapiGetRecordId();
	var searchString1 = 'PKGL-WSP-RT';
	var searchString2 = 'PKGM-WSP-RT';
	var searchString3 = 'PKGS-WSP-RT';
	
	var largeKitSubId = 36;
	var mediumKitSubId = 33;
	var smallKitSubId = 23;
	var canadaSub = 4;
	var canadaLocation = 1;
	var usaLocation = 2;
	
	if (nlapiGetContext().getEnvironment() != 'PRODUCTION') {	
		largeKitSubId = 36;
		mediumKitSubId = 33;
		smallKitSubId = 23;
		canadaSub = 4;
		canadaLocation = 1;
		usaLocation = 2;
	}
	
	var validTran = false;
	var rec = null, itemId = null;
		
	if (salesOrderId != null) {
		
		var itemModified = false;
		
		validTran = true;
		try {
			rec = nlapiLoadRecord('salesorder', salesOrderId);
		} catch(e) {
			validTran = false; 
		}
		
		if (validTran) {
			itemModified = true;
			var itemCount = rec.getLineItemCount('item');
			var subsiderary = rec.getFieldValue('subsidiary');
			nlapiLogExecution('debug','subsiderary', subsiderary);
			
			if (subsiderary == canadaSub)
				rec.setFieldValue('location', canadaLocation);
			else
				rec.setFieldValue('location', usaLocation);
			
			rec.setFieldValue('custbody_autocreatedfromquote', 'T');
			
			if (itemCount > 0) {
				
				for (var item = 1; item <= itemCount; item++) {
					
					nlapiLogExecution('debug','itemId', itemId);
					itemId = nlapiLookupField('item',rec.getLineItemValue('item','item',item),'itemid');
					
					if (itemId != null && itemId.indexOf(searchString1) > -1) {
						rec.selectNewLineItem('item');
						rec.setCurrentLineItemValue('item', 'item', largeKitSubId);
						rec.setCurrentLineItemValue('item', 'quantity', rec.getLineItemValue('item','quantity',item));
						
						rec.commitLineItem('item');
					}
					
					if (itemId != null && itemId.indexOf(searchString2) > -1) {
						rec.selectNewLineItem('item');
						rec.setCurrentLineItemValue('item', 'item', mediumKitSubId);
						rec.setCurrentLineItemValue('item', 'quantity', rec.getLineItemValue('item','quantity',item));
						
						rec.commitLineItem('item');
					}

					if (itemId != null && itemId.indexOf(searchString3) > -1) {
						rec.selectNewLineItem('item');
						rec.setCurrentLineItemValue('item', 'item', smallKitSubId);
						rec.setCurrentLineItemValue('item', 'quantity', rec.getLineItemValue('item','quantity',item));
						
						rec.commitLineItem('item');
					}
				}
			}
			
			if (itemModified)
				nlapiSubmitRecord(rec, true);
					
		}
		
	}
	
}

function setOpportunityStage(opportunityStage) {
	
	var newOpportunityStage = nlapiGetContext().getSetting('SCRIPT', 'custscript_opportunitystage');
	var salesOrderId = nlapiGetRecordId();
	var opportunityId = null, entitystatus = null;
	var validTran = false;
	
	if (salesOrderId != null) {
			
		validTran = true;
		try {
			opportunityId = rec.getFieldValue('opportunity');
		} catch(e) {
			validTran = false; 
		}
			
		if (validTran) {
			validTran = true;
			try {
				if (opportunityId != null && opportunityId.length > 0)
					rec = nlapiLoadRecord('opportunity', opportunityId);
				else
					validTran = false; 
			} catch(e) {
				validTran = false; 
			}
			
			if (validTran) {
				entitystatus = rec.getFieldValue('entitystatus');
				if (entitystatus != newOpportunityStage) {
					rec.setFieldValue('entitystatus', newOpportunityStage);
					nlapiSubmitRecord(rec, true);
				}
				
			}

		}
		
	}
	
}

