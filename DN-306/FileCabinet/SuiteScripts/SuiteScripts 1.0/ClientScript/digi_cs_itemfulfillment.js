/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Nov 2016     krobi
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */

function onclick_callClient(){
  	var url = nlapiResolveURL('SUITELET', 'customscript_digi_sl_simentryform', 'customdeploy_digi_sl_simentryform')+'&digi_itmfulid=' + nlapiGetRecordId() + '&popup=t&recordType=' + nlapiGetRecordType();
  	nlExtOpenWindow(url, '', 600, 600, '', false, 'Sim Entry Form');
}
