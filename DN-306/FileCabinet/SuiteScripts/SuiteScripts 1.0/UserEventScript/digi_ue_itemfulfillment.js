/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Nov 2016     krobi
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	
	var id = null;
	try {
		id = request.getParameter('id');
	} catch(e) {
		id = null;
	}
	
	if (id != null) {
		var recordId = nlapiGetRecordId();
		
		var validItemFound = false;
		for (var i = 1; i <= nlapiGetLineItemCount('item'); i++) {
			
			var itemId = nlapiGetLineItemValue('item', 'item', i);
			//nlapiLogExecution('DEBUG', 'i' , itemId);
			
			if (itemId == 181) {
				validItemFound = true;
				break;
			}
		}
		
		if (recordId != null && validItemFound) {
			form.addButton('custpage_buttonsimids', 'Add Sim IDs', 'onclick_callClient()');
			form.setScript('customscript_digi_cs_itemfulfillment');
		}
			
	}
}