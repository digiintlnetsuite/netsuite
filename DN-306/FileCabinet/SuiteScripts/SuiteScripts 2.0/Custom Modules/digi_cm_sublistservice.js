define(['N/record'],

function(record) {
   
	function itemExits(context, findItemId) {

    	var itemCount = context.getLineCount({
    		sublistId: 'item'
    	});
    	   	
		for (var item = 0; item < itemCount; item++) {
        	var itemId = context.getSublistValue({
        		sublistId: 'item',
        		fieldId: 'item',
        		line: item
        	});
        	
        	if (findItemId == itemId)
        		return true;
    	}
    	
    	return false;
	}
		
    return {
    	itemExits : itemExits
    };
    
});
