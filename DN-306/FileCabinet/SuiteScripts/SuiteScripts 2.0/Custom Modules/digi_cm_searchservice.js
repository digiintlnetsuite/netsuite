define(['N/search'],

function(search) {
	
	function simIdSearch(itemFulfillId) {
        var simidSearch = search.create({
        	type: 'customrecord_digi_simids',
            columns: [{
                name: 'custrecord_digi_simid',
            }, {
            	name: 'custrecord_digi_serial_number'
            }, {
            	name: 'custrecord_digi_itemfulfillment'
            }, {
            	name: 'internalid'
            }],
            filters: [{
                name: 'custrecord_digi_itemfulfillment',
                operator: 'is',
                values: itemFulfillId
            }]
        });
        
        return simidSearch;
	}
	
	function searchInventoryDetail(itemFulfillId, itemSearchArray) {
		
		var salesOrderSearch = search.create({
	        type: search.Type.ITEM_FULFILLMENT,
	        columns: [{
	            name: 'inventorynumber',
	            join : 'inventoryDetail',
	            label : 'inventory number'
	        }, {
	        	name: 'mainline'
	        }, {
	        	name: 'Type'
	        }, {
	        	name: 'internalid',
	        	join: 'item'
	        }],
	        filters: [{
	            name: 'internalid',
	            join: 'item',
	            operator: 'anyof',
	            values: itemSearchArray
	        }, {
	            name: 'internalid',
	            operator: 'is',
	            values: itemFulfillId
	        }]
        });
        
        return salesOrderSearch;
	}
	
    return {
    	simIdSearch : simIdSearch,
    	searchInventoryDetail : searchInventoryDetail
    };
    
});
