/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */

require.config({
	paths : {
		"searchservice": "/SuiteScripts/SuiteScripts 2.0/Custom Modules/digi_cm_searchservice"
	},
});

define(['N/encode', 'N/runtime', 'N/ui/serverWidget', 'N/url', 'N/error', 'N/record', 'N/search', 'N/redirect', 'searchservice'],

function(encode, runtime, serverWidget, url, error, record, search, redirect, searchservice) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {

        try {
        	var simItemId = '181';
            var request = context.request;
            var response = context.response;
        	
            if (context.request.method == 'GET') {
            	
            	var itemFulfillId = request.parameters.digi_itmfulid;
            	
            	var popup;
            	try {
            		if (!request.parameters.popup)
            			popup = 'f';
            		else
            			popup = request.parameters.popup;
            	} catch (e) {
            		popup = 'f';
            	}
            	
            	var form = serverWidget.createForm({
                    title: 'Sim Entry Form'
                });
            	
                var itmfulfillIdField = form.addField({
	                id: 'digi_itmfulid',
	                type: serverWidget.FieldType.TEXT,
	                label: 'internalId'
                });
                itmfulfillIdField.defaultValue = itemFulfillId;
                if (itmfulfillIdField) {
                	itmfulfillIdField.updateDisplayType({
                        displayType: serverWidget.FieldDisplayType.HIDDEN
                    });
                }
                
                var popupField = form.addField({
	                id: 'popup',
	                type: serverWidget.FieldType.TEXT,
	                label: 'popup'
                });
                popupField.defaultValue = popup;
                if (popupField) {
                	popupField.updateDisplayType({
                        displayType: serverWidget.FieldDisplayType.HIDDEN
                    });
                }
                                
                var sublist = form.addSublist({
                    id: 'sublistid',
                    type: serverWidget.SublistType.INLINEEDITOR,
                    label: 'SIM Card Detail'
                });
                
                var internalIdField = sublist.addField({
                    id: 'internalid',
                    type: serverWidget.FieldType.TEXT,
                    label: 'Id',
                    displayType: serverWidget.FieldDisplayType.HIDDEN
                });
                if (internalIdField) {
                	internalIdField.updateDisplayType({
                        displayType: serverWidget.FieldDisplayType.HIDDEN
                    });
                }
                
                var serialNumberField = sublist.addField({
                    id: 'serialnumberid',
                    type: serverWidget.FieldType.TEXT,
                    label: 'Serial Number'
                });
                if (serialNumberField) {
                	serialNumberField.updateDisplayType({
                        displayType: serverWidget.FieldDisplayType.DISABLED
                    });
                }
                serialNumberField.isMandatory = true;
                
                var simIdField = sublist.addField({
                    id: 'simid',
                    type: serverWidget.FieldType.TEXT,
                    label: 'Sim ID'
                });
                simIdField.isMandatory = true;
                
                form.addSubmitButton({
                    label: 'Save'
                });
            	
        		context.response.writePage(form);
        		      		
        		var simidSearch = searchservice.simIdSearch(itemFulfillId);
                var counter = 0;
                var filterArray = new Array();
                simidSearch.run().each(function(result) {
                    sublist.setSublistValue({
                        id : 'internalid',
                        line : counter,
                        value : result.getValue('internalid')
                    });
                	
                    if (result.getValue('custrecord_digi_serial_number')) {
	                    sublist.setSublistValue({
	                        id : 'serialnumberid',
	                        line : counter,
	                        value : result.getValue('custrecord_digi_serial_number')
	                    });
	                    
	                    filterArray.push(result.getValue('custrecord_digi_serial_number'));
                    }
                	
                    if (result.getValue('custrecord_digi_simid')) {
	                    sublist.setSublistValue({
	                        id : 'simid',
	                        line : counter,
	                        value : result.getValue('custrecord_digi_simid')
	                    });
                    }
                    
                    counter++;
                	return true;
            	});
                        
        		var itemSearchArray = new Array();
        		itemSearchArray.push(simItemId);
            	var searchInventoryDetail = searchservice.searchInventoryDetail(itemFulfillId, itemSearchArray);
                
            	searchInventoryDetail.run().each(function(result) {
                    var serialNumberText = result.getText({name: 'inventorynumber', join: 'inventoryDetail'});
                	log.debug('serialNumberText',result);
                    
            		if (serialNumberText && filterArray.indexOf(serialNumberText) == -1) {
	            		sublist.setSublistValue({
	                        id : 'serialnumberid',
	                        line : counter,
	                        value : serialNumberText
	                    });
            		}
                	
                    counter++;
                	return true;
                });
            	
        		return;
            }
            
            var lineCount = request.getLineCount({group: "sublistid" });
            var itemFulfillId = request.parameters.digi_itmfulid;
            var popup = request.parameters.popup;

        	for (var item = 0; item < lineCount; item++) {
        		
                var internalId = request.getSublistValue({
                	group: "sublistid",
                	name: "internalid",
                    line: item
                });
        		
                var serialNumber = request.getSublistValue({
                	group: "sublistid",
                	name: "serialnumberid",
                    line: item
                });
                
                var simId = request.getSublistValue({
                	group: "sublistid",
                	name: "simid",
                    line: item
                });

                if (!internalId) {
                	
                    var objRecord = record.create({
                    	type: 'customrecord_digi_simids', 
                    	isDynamic: true
                    });

                    if (serialNumber) {  
	                    objRecord.setValue({
	                    	fieldId: 'custrecord_digi_serial_number',
	                    	value: serialNumber
	                    });
                    }

                    if (simId) {                  	
	                    objRecord.setValue({
	                    	fieldId: 'custrecord_digi_simid',
	                    	value: simId
	                    });
                	}
                    
                    if (itemFulfillId) { 
	                    objRecord.setValue({
	                    	fieldId: 'custrecord_digi_itemfulfillment',
	                    	value: itemFulfillId
	                    });
                    }
           
                    var recordId = objRecord.save({
                    	enableSourcing : false,
                    	ignoreMandatoryFields : true
                    });
                	
                } else {
                    var objRecord = record.load({
                    	type: 'customrecord_digi_simids',
                    	id: internalId,
                    	isDynamic: true
                    });

                    if (serialNumber) { 
	                    objRecord.setValue({
	                    	fieldId: 'custrecord_digi_serial_number',
	                    	value: serialNumber
	                    });
                    }
                
                    if (simId) { 
	                    objRecord.setValue({
	                    	fieldId: 'custrecord_digi_simid',
	                    	value: simId
	                    });
                    }
                    
                    var recordId = objRecord.save({
                    	enableSourcing : false,
                    	ignoreMandatoryFields : true
                    });
                }
        	}

        	if (popup != 't') {
	    		redirect.toRecord({
	    			type : record.Type.ITEM_FULFILLMENT,
	    			id : itemFulfillId
	    		});
        	} else {
            	response.write('<script type="text/javascript">window.parent.location.reload();</script>');
        	}
        	
            return;
        } catch (e) {
            var ex = JSON.parse(e);
            var errorType = ex.type;
            var errorMsg = 	'Error: ' + ex.name + '\n' +
            				'Message: ' + ex.message;
            
            if (errorType == 'error.SuiteScriptError') {
            	errorMsg = errorMsg + '\n' +
            				'ID: ' + ex.Id + '\n' +
            				'Cause: ' + ex.cause + '\n' +
            				'Stack Trace: ' + ex.stack;
            }
            
            if (errorType == 'error.UserEventError') {
            	errorMsg = errorMsg + '\n' +
            				'ID: ' + ex.Id + '\n' +
            				'Event Type: ' + ex.eventType + '\n' +
            				'Record ID: ' + ex.recordId + '\n' +
            				'Stack Trace: ' + ex.stack;
            }
            
            log.debug(errorType, errorMsg);
        }
    	
    }

    return {
        onRequest: onRequest
    };
    
});
